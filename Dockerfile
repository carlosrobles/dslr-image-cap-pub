from arm32v7/python:3.7.2-stretch

MAINTAINER carlos.robles@gmail.com

COPY Pipfile Pipfile.lock /app/

WORKDIR /app

RUN apt-get update && \
    apt-get install -y \
        gcc \
        libltdl-dev \
        libusb-dev \
        libexif-dev \
        libpopt-dev \
        wget && \
    wget http://ftp.de.debian.org/debian/pool/main/libu/libusbx/libusbx_1.0.11.orig.tar.bz2 && \
    tar xjf libusbx_1.0.11.orig.tar.bz2 && \
    cd libusbx-1.0.11 && \
    ./configure && \
    make && \
    make install && \
    cd .. && \
    wget https://sourceforge.net/projects/gphoto/files/libgphoto/2.5.20/libgphoto2-2.5.20.tar.bz2 && \
    tar xjf libgphoto2-2.5.20.tar.bz2 && \
    cd libgphoto2-2.5.20 && \
    ./configure && \
    make && \
    make install && \
    cd .. && \
    wget https://sourceforge.net/projects/gphoto/files/gphoto/2.5.20/gphoto2-2.5.20.tar.bz2 && \
    tar xjf gphoto2-2.5.20.tar.bz2 && \
    cd gphoto2-2.5.20 && \
    ./configure && \
    make && \
    make install && \
    cd .. && \
    ldconfig && \
    rm -rf \
        libusbx_1.0.11.orig.tar.bz2 \
        libusbx-1.0.11 \
        libgphoto2-2.5.20.tar.bz2 \
        libgphoto2-2.5.20 \
        gphoto2-2.5.20.tar.bz2 \
        gphoto2-2.5.20 && \
    pip install pipenv && \
    pipenv install --deploy --system

COPY dslr-image-cap-pub/ /app/

CMD tail -f /dev/null

