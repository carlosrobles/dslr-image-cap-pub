from threading import Timer
import argparse
import os
import sys
import time
import gphoto2 as gp

script_name = 'app.py'

# parse positional and optional arguments
def parseargs():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description='This python script uses gphoto2 to capture images on a connected camera every x seconds for y seconds',
        epilog='''
Examples:
  %(script_name)s -i 60 -d 86400                     Run a capture every 60 seconds for 24 hours
  %(script_name)s --interval c160 --duration 86400''' % {'script_name': script_name}
              )
    parser.add_argument('-i','--interval', dest='interval', type=int, help='How often to take a picture (in seconds)')
    parser.add_argument('-d','--duration', dest='duration', type=int, help='How long you want to capture images (in seconds)')
    return parser.parse_args()

def initiate_camera():
    # Initiate camera
    context = gp.gp_context_new()
    camera = gp.check_result(gp.gp_camera_new())
    gp.check_result(gp.gp_camera_init(camera, context))
    return camera, context

def capture_image():
    # Capture image
    print('Capturing image')
    file_path = gp.check_result(gp.gp_camera_capture(camera, gp.GP_CAPTURE_IMAGE))
    print('Camera capture path: %s/%s' % (file_path.folder, file_path.name))

    # Copy image
    target = os.path.join('/tmp', file_path.name)
    print('Copying image to', target)
    camera_file = gp.check_result(gp.gp_camera_file_get(camera,file_path.folder, file_path.name, gp.GP_FILE_TYPE_NORMAL))
    print('Finished copying image to', target)

    # unclear if this is needed
    #print('Deleting file from camera')
    #file_delete = gp.check_result(gp.gp_camera_file_delete(camera, file_path.folder, file_path.name))
    #print('Deleted file from camera: %s/%s' % (file_path.folder, file_path.name))

    print('Checking camera exit result')
    gp.check_result(gp.gp_camera_exit(camera, context))
    print('Finished checking camera exit result')

def run_timelapse(interval, end_time):
    now = time.time()
    if now < end_time:
        capture_image()
        t = Timer(interval, run_timelapse, args=[interval, end_time])
        t.start()

if __name__ == "__main__":
    # Get input args
    args = parseargs()
    camera, context = initiate_camera()
    start_time = time.time()
    end_time = start_time + args.duration
    run_timelapse(args.interval, end_time)
